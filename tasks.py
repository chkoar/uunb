from invoke import task


@task
def sort_imports(ctx):
    ctx.run("isort uunb tests --recursive")


@task
def lint(ctx):
    ctx.run("black --check uunb tests")
    ctx.run("flake8 uunb tests")


@task
def test(ctx):
    ctx.run("pytest -v -x --cov --cov-report term-missing")
