#! /usr/bin/env python

import codecs
import os

from setuptools import find_packages, setup


DISTNAME = "uunb"
DESCRIPTION = "Uncertainty Based Under-Sampling for Learning Naive Bayes Classifiers Under Imbalanced Data Sets"
with codecs.open("README.md", encoding="utf-8-sig") as f:
    LONG_DESCRIPTION = f.read()
MAINTAINER = "Christos K. Aridas"
MAINTAINER_EMAIL = "chkoar@users.noreply.github.com"
URL = "https://gitlab.com/chkoar/uunb"
LICENSE = "new BSD"
DOWNLOAD_URL = "https://gitlab.com/chkoar/uunb"
VERSION = "0.1.0"
INSTALL_REQUIRES = [
    "imbalanced-learn",
    "modAL",
]
CLASSIFIERS = [
    "Intended Audience :: Science/Research",
    "Intended Audience :: Developers",
    "Programming Language :: Python",
    "Topic :: Software Development",
    "Topic :: Scientific/Engineering",
    "Operating System :: Microsoft :: Windows",
    "Operating System :: POSIX",
    "Operating System :: Unix",
    "Operating System :: MacOS",
    "Programming Language :: Python :: 3.6",
    "Programming Language :: Python :: 3.7",
]
EXTRAS_REQUIRE = {
    "tests": ["black", "flake8", "invoke", "isort", "pytest", "pytest-cov",]
}

setup(
    name=DISTNAME,
    maintainer=MAINTAINER,
    maintainer_email=MAINTAINER_EMAIL,
    description=DESCRIPTION,
    license=LICENSE,
    url=URL,
    version=VERSION,
    download_url=DOWNLOAD_URL,
    long_description=LONG_DESCRIPTION,
    zip_safe=False,  # the package can run out of an .egg file
    classifiers=CLASSIFIERS,
    packages=find_packages(),
    install_requires=INSTALL_REQUIRES,
    extras_require=EXTRAS_REQUIRE,
)
