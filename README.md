
# UUNB

Python package that implements the method that is presented in the paper entitled `Uncertainty based under-sampling for learning Naive Bayes classifiers under imbalanced data sets`.

Free software: 3-clause BSD license

## An example using only the resampling process

-------

```python
from collections import Counter

from uunb import UUNB
from sklearn.datasets import make_classification

# Load the data set
X, y = make_classification(random_state=0, n_samples=1000, weights=[.9, .1])

print(Counter(y))

sampler = UUNB(random_state=0)

# Run the method
X_resampled, y_resampled = sampler.fit_resample(X,y)

print(Counter(y_resampled))
```

## An example that learns a Naive Bayes model after the resampling of the proposed approach

-------
```python
from uunb import UUNB
from sklearn.datasets import load_digits
from sklearn.metrics import classification_report
from sklearn.naive_bayes import GaussianNB
from imblearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split

# Load the data set
X, y = load_digits(return_X_y=1)

# Make it binary
y[y!=1]=0

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=0)

# Train the models on the traing data set
plain_nb = GaussianNB().fit(X_train, y_train)
ais_nb = make_pipeline(UUNB(random_state=0, verbose=True, n_queries=400), GaussianNB()).fit(X_train, y_train)

results_plain_nb = classification_report(y_test, plain_nb.predict(X_test))
results_ais_nb = classification_report(y_test, ais_nb.predict(X_test))

# Prints the result
print(results_plain_nb)
print(results_ais_nb)
```

## Dependencies


* scipy
* numpy
* scikit-learn
* imbalanced-learn
* modAL

## Installation

`pip install -U git+https://gitlab.com/chkoar/uunb.git`