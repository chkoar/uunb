from collections import Counter

import numpy as np
from imblearn.over_sampling import SMOTE
from modAL.models import ActiveLearner
from modAL.uncertainty import uncertainty_sampling
from sklearn import utils
from sklearn.base import clone, BaseEstimator
from sklearn.metrics import balanced_accuracy_score
from sklearn.naive_bayes import GaussianNB


class UUNB(BaseEstimator):
    """Class to perform active selection and generation of instances as described in paper.
    ----------
    n_queries : int, optional (default=200)
        The number of active selection queries to be performed (q parameter in paper).
    n_init : int, optional (default=0.5)
        The ratio of the minority class (r paramter in paper).
    threshold : float, optional (default=1.0)
        The balanced accuracy threshold in order to terminate the active selection
        process (t parameter in paper).
    n_neighbors : int, optional (default=5)
        The nearest neighbors to be used by SMOTE (k parameter in paper).
    random_state : int, RandomState instance or None, optional (default=None)
        The seed of the pseudo random number generator to use when shuffling
        the data.  If int, random_state is the seed used by the random number
        generator; If RandomState instance, random_state is the random number
        generator; If None, the random number generator is the RandomState
        instance used by `np.random`.
    verbose : boolean, optional (default=False)
        Whether to print messages about the resampling process.

    Examples
    --------
    >>> from collections import Counter
    >>> from sklearn.datasets import make_classification
    >>> from uunb import UUNB
    >>> X, y = make_classification(n_classes=2, class_sep=2,
    ...  weights=[0.1, 0.9], n_informative=3, n_redundant=1, flip_y=0,
    ... n_features=20, n_clusters_per_class=1, n_samples=1000, random_state=10)
    >>> print('Original dataset shape %s' % Counter(y))
    Original dataset shape Counter({1: 900, 0: 100})
    >>> sampler = UUNB(random_state=0)
    >>> X_res, y_res = sampler.fit_resample(X, y)
    >>> print('Resampled dataset shape %s' % Counter(y_res))
    Resampled dataset shape Counter({1: 65, 0: 53})
    """

    def __init__(
        self,
        n_queries=200,
        n_init=0.5,
        threshold=1.0,
        n_neighbors=5,
        random_state=None,
        verbose=False,
    ):
        self.n_queries = n_queries
        self.n_init = n_init
        self.threshold = threshold
        self.n_neighbors = n_neighbors
        self.random_state = random_state
        self.verbose = verbose

    def fit_resample(self, X, y):

        np.random.seed(self.random_state)
        random_state = utils.check_random_state(self.random_state)

        self.estimator_ = GaussianNB()

        counter = Counter(y)
        n_samples = counter.get(min(counter, key=counter.get))

        n_init = self._validate_n_init(n_samples)
        threshold = self._validate_threshold()

        init_idxs = []

        for label in np.unique(y):
            idx = random_state.choice(np.where(y == label)[0], n_init)
            init_idxs.append(idx)

        init_idxs = np.concatenate(init_idxs)

        if self.verbose:
            print("Initial training indices: {}".format(init_idxs))

        X_training = X[init_idxs]
        y_training = y[init_idxs]

        X_pool = np.delete(X, init_idxs, axis=0)
        y_pool = np.delete(y, init_idxs)

        smote = SMOTE(k_neighbors=self.n_neighbors, random_state=random_state)
        try:
            X_pool, y_pool = smote.fit_resample(X_pool, y_pool)
        except Exception as ex:
            msg = ex.args[0]
            msg = msg.replace("k_neighbors", "n_neighbors")
            raise ValueError(msg)

        model = ActiveLearner(
            clone(self.estimator_),
            uncertainty_sampling,
            X_training=X_training,
            y_training=y_training,
        )

        preds = model.predict(X_pool)
        bacc = balanced_accuracy_score(y_pool, preds)

        if self.verbose:
            print("Initial balanced accuracy:{}.".format(bacc))

        for i, _ in enumerate(range(self.n_queries)):
            try:
                query_idx, _ = model.query(X_pool, random_tie_break=True)
                X_new = X_pool[query_idx].reshape(1, -1)
                y_new = y_pool[query_idx].reshape(1)

                model.teach(X=X_new, y=y_new)

                X_pool = np.delete(X_pool, query_idx, axis=0)
                y_pool = np.delete(y_pool, query_idx)

                preds = model.predict(X_pool)
                bacc = balanced_accuracy_score(y_pool, preds)
            except ValueError:
                if self.verbose:
                    print("The pool is empty.")
                    break

            if self.verbose:
                print("Iteration: {}. Balanced accuracy:{}.".format(i, bacc))

            if bacc >= threshold:
                break

        return model.X_training, model.y_training

    def fit(self, X, y):
        return self

    def _validate_n_init(self, n_samples):
        if isinstance(self.n_init, np.int):
            n_init = self.n_init
        elif isinstance(self.n_init, np.float):
            n_init = int(n_samples * self.n_init)
        else:
            raise ValueError("n_samples paremeter must be either int or float")
        return n_init

    def _validate_threshold(self):
        msg = "threshold parameter must be a float value between 0 and 1"
        try:
            if self.threshold < 0 or self.threshold > 1:
                raise ValueError(msg)
        except Exception:
            raise ValueError(msg)
        else:
            return self.threshold
