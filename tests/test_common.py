import pytest
from collections import Counter, namedtuple
from imblearn.pipeline import make_pipeline
from sklearn.datasets import load_digits
from sklearn.datasets import make_classification
from sklearn.naive_bayes import GaussianNB

from uunb import UUNB


@pytest.fixture
def dataset():
    Dataset = namedtuple("Dataset", "X y")
    X, y = make_classification(random_state=0, n_samples=1000, weights=[0.9, 0.1])
    return Dataset(X=X, y=y)


def test_default(dataset):
    sampler = UUNB(random_state=0)
    X_resampled, y_resampled = sampler.fit_resample(dataset.X, dataset.y)
    counter = Counter(y_resampled)
    assert counter[0] == 147
    assert counter[1] == 155


def test_fit(dataset):
    sampler = UUNB(random_state=0)
    sampler_after_fit = sampler.fit(dataset.X, dataset.y)
    assert id(sampler) == id(sampler_after_fit)


@pytest.mark.parametrize("n_queries, should_not_raise", [(1, True), (1000, True)])
def test_n_queries(dataset, n_queries, should_not_raise):
    sampler = UUNB(random_state=0, n_queries=n_queries)
    sampler.fit_resample(dataset.X, dataset.y)


@pytest.mark.parametrize(
    "n_init, should_not_raise", [(0.5, True), (10, True), ("foo", False)]
)
def test_n_init(dataset, n_init, should_not_raise):
    sampler = UUNB(random_state=0, n_init=n_init)
    if should_not_raise:
        sampler.fit_resample(dataset.X, dataset.y)
    else:
        with pytest.raises(ValueError):
            sampler.fit_resample(dataset.X, dataset.y)


@pytest.mark.parametrize(
    "n_neighbors, should_not_raise", [(0.5, False), (10, True), ("foo", False)]
)
def test_n_neighbors(dataset, n_neighbors, should_not_raise):
    sampler = UUNB(random_state=0, n_neighbors=n_neighbors)
    if should_not_raise:
        sampler.fit_resample(dataset.X, dataset.y)
    else:
        with pytest.raises(ValueError):
            sampler.fit_resample(dataset.X, dataset.y)


@pytest.mark.parametrize(
    "threshold , should_not_raise", [(0.5, True), (10, False), ("foo", False)]
)
def test_threshold(dataset, threshold, should_not_raise):
    sampler = UUNB(random_state=0, threshold=threshold)
    if should_not_raise:
        sampler.fit_resample(dataset.X, dataset.y)
    else:
        with pytest.raises(ValueError):
            sampler.fit_resample(dataset.X, dataset.y)


def test_pipeline():
    X, y = load_digits(return_X_y=1)
    y[y != 1] = 0  # Make it binary
    sampler = UUNB(random_state=0, verbose=True, n_queries=400)
    model = GaussianNB()
    pipeline = make_pipeline(sampler, model)
    pipeline.fit(X, y)
